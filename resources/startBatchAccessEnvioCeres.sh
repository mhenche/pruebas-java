#!/bin/bash

HOME_COMUN=/servicios/springboot/comun-config
HOME_BCH=/servicios/springboot/batch
#BCH_NAME=bch-access
BCH_NAME=testBatchHenche

BCH_VERSION=2.10.0
BCH_LOG=/servicios/springboot/logs/batch/$BCH_NAME/$BCH_VERSION/instance1/*.log
hayProceso=`ps | grep -v grep | grep -v PID | grep $BCH_NAME | awk '{ priont $2}' | wc -l`

#/servicios/springboot/jdk18/jdk1.8.0_77/bin/java -Xms128m -Xmx256m 
#-Dspring.config.location=$HOME_COMUN/applicationbatch_COMUN.yml,$HOME_BCH/$BCH_NAME/$BCH_VERSION/instance1/application_PRO.yml 
#-Djavax.net.ssl.trustStore=/servicios/springboot/comun-config/certmicroservicespro.jks -Djavax.net.ssl.trustStorePassword=test 
#-jar $HOME_BCH/$BCH_NAME/$BCH_VERSION/instance1/$BCH_NAME-$BCH_VERSION.jar --proceso=envio_ceres


java -jar TestStartProcessShell.jar > /home/henche/logging-$BCH_NAME-"$dt".log


linea=0
while [ $hayProceso -gt 0 ]
do
		
        sleep 120
        linea=`awk -v linea=$linea '{ if (NF > linea && $0 ~ ERROR && $0 ~ Exception ) { exit 2 } } END { print NF}' $BCH_LOG`
        if [ $? -eq 2 ]
        then
                exit 2
        fi
        hayProceso=`ps | grep -v grep | grep -v PID | grep $BCH_NAME | awk '{ priont $2}' | wc -l`
done

exit $?
