#!/bin/bash

ENTORNO="PRO"
BCH_VERSION="1.5.0"
MEMORIA="-Xmx256m -Xmx512m"

JDK_HOME="/servicios/springboot/jdk18/jdk1.8.0_77"
HOME_COMUN="/servicios/springboot/comun-config"
HOME_BCH="/servicios/springboot/batch"
HOME_LOGS="/servicios/springboot/logs/batch"
BCH_NAME="TestStartProcessShell"
TRUST_STORE_FILE="/servicios/springboot/comun-config/certmicroservicespro.jks"
TRUST_STORE_PASS="test"

dt=$(date +"%a")

#nohup $JDK_HOME/bin/java $MEMORIA -Dspring.config.location=$HOME_COMUN/application_GLOBAL.yml,$HOME_COMUN/applicationbatch_COMUN.yml,$HOME_BCH/$BCH_NAME/$BCH_VERSION/application_$ENTORNO.yml,$HOME_BCH/$BCH_NAME/application_PROPIERTIES.yml -Djavax.net.ssl.trustStore=$TRUST_STORE_FILE -Djavax.net.ssl.trustStorePassword=$TRUST_STORE_PASS -jar $HOME_BCH/$BCH_NAME/$BCH_VERSION/instance1/$BCH_NAME-$BCH_VERSION.jar >/dev/null 2>&1 &

java -jar TestStartProcessShell.jar > /home/henche/logging-$BCH_NAME-"$dt".log

exit $?