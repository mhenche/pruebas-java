package mhenche.test;


public class RunnableDemo implements Runnable {
	   private Thread t;
	   private String nombreHilo;
	   private long tiempoResposo;
	   private boolean continuar = true;
	   
	   RunnableDemo( String name, long tiempoResposo) {
	      this.nombreHilo = name;
	      this.tiempoResposo = tiempoResposo;
	   }
	   
	   public void operacionCostosa1() {
		   double salida = 0d;
		   for(int i =0; i<1000; i++) {
			   salida += Math.random();
		   }
		   //System.out.println("Obtenemos" +  salida);
		   
	   }
	   
	   public void operacionCostosa2() {
		   double salida = 0d;
		   for(int i =0; i<10000; i++) {
			   salida += Math.random();
		   }
		   //System.out.println("Obtenemos" +  salida);
		   
	   }
	   
	   public void run() {
		  System.out.println("Arrancando un nuevo hilo:" +  nombreHilo );
	      try {
	         while(isContinuar()){
	        	 operacionCostosa1();
	        	 operacionCostosa2();
	        	 Thread.sleep(tiempoResposo);
	         }
	      }
	      catch (Exception e) {
		         System.out.println("Thread " +  nombreHilo + " interrupted.");
		      }
	      System.out.println("Thread " +  nombreHilo + " exiting.");
	   }
	   
	   public void start () {
	      System.out.println("Starting " +  nombreHilo );
	      if (t == null) {
	         t = new Thread (this, nombreHilo);
	         t.start ();
	      }
	   }

	public boolean isContinuar() {
		return continuar;
	}

	public void setContinuar(boolean continuar) {
		this.continuar = continuar;
	}
	}