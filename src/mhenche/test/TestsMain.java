package mhenche.test;

import java.util.concurrent.TimeUnit;

public class TestsMain {

	public TestsMain() {
	}

	public static void main(String[] args) {

//		System.out.println(String.format("Pruebas de henche: %1$.3f", 1.21312939123));
//				
//		System.out.println(String.format(
//				"Usaremos el nombre de fichero \"%-30s\" para el fichero con nombre original: \"%s\" ","final name", "original name"));

		printTime(36016999);
		
	}
	
	public static void printTime(long millis) {
		String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println(hms);
		
	}
}
