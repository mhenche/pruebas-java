package mhenche.test;

import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.Scanner;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

public class TestThread {
	public static void main(String args[]) throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, MalformedObjectNameException {
		
		
		TestThread testThread = new TestThread();
		testThread.ejemploMBean();
		testThread.arrancarhilos();


	}
	
	public void arrancarhilos() {
		
		//ExecutorService executor = Executors.newFixedThreadPool(2);
		
		
		
				int hiloNum = 0;
				
				ThreadGroup group = Thread.currentThread().getThreadGroup();
				

//				(new RunnableDemo("HiloChungo-"+hiloNum, 5)).start();


				boolean loop_cond = true;

				Scanner entradaEscaner = new Scanner(System.in); 
				while (loop_cond) {
					String entradaTeclado = "";
					entradaTeclado = entradaEscaner.nextLine(); // Invocamos un método sobre
					
					if(entradaTeclado.equalsIgnoreCase("salir")) {
						group.interrupt();
						loop_cond = false;
					}else {
						if(entradaTeclado.contains("add")) {
							hiloNum++;
							String[] datos = entradaTeclado.split(",");
							try {
								(new RunnableDemo("HiloChungo-"+hiloNum, Long.parseLong(datos[1]))).start();
							} catch (Exception e) {
								System.out.println("Error añadiendo Hilo. Usa el formato add name,tiempoReposo");
							}
							
						}else if(entradaTeclado.equalsIgnoreCase("listar")) {
							Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
							for(Map.Entry<Thread, StackTraceElement[]> entry : allStackTraces.entrySet()) {
								System.out.println(entry.getKey().getName());
							}
						}
						else if(entradaTeclado.equalsIgnoreCase("2")) {
							Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
							for(Map.Entry<Thread, StackTraceElement[]> entry : allStackTraces.entrySet()) {
								if(entry.getKey().getName().equals("HiloChungo-"+hiloNum)) {
									entry.getKey().interrupt();
									hiloNum--;
									break;
								}
							}
						}
					}

				}
				entradaEscaner.close();
		
	}
	
	public void ejemploMBean() throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, MalformedObjectNameException {
		
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer(); 
        ObjectName name = new ObjectName("mhenche.test:type=HelloBean"); 
        MbeanDemo mbean = new MbeanDemo(); 
        mbs.registerMBean(mbean, name); 
          
        
     
        		
		
	}
}
