package mhenche.test;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.ReflectionException;

public class MbeanDemo implements  DynamicMBean {

	public MbeanDemo() {

	}

	public void sayHello() {
		System.out.println("hello, world");
	}

	public int add(int x, int y) {
		return x + y;
	}

	public String getName() {
		return this.name;
	}

	public int getCacheSize() {
		return this.cacheSize;
	}

	public synchronized void setCacheSize(int size) {

		this.cacheSize = size;
		System.out.println("Cache size now " + this.cacheSize);
	}

	private final String name = "Reginald";
	private int cacheSize = DEFAULT_CACHE_SIZE;
	private static final int DEFAULT_CACHE_SIZE = 200;
	
	
	@Override
	public Object getAttribute(String attribute)
			throws AttributeNotFoundException, MBeanException,
			ReflectionException {
		
		return null;
	}

	@Override
	public void setAttribute(Attribute attribute)
			throws AttributeNotFoundException, InvalidAttributeValueException,
			MBeanException, ReflectionException {
		
		
	}

	@Override
	public AttributeList getAttributes(String[] attributes) {
		AttributeList attributeList = new AttributeList();
		Attribute attribute = new Attribute("atributo1", "valor del atributo 1");
		attributeList.add(attribute);
		return attributeList;
	}

	@Override
	public AttributeList setAttributes(AttributeList attributes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object invoke(String actionName, Object[] params, String[] signature)
			throws MBeanException, ReflectionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MBeanInfo getMBeanInfo() {
		
		
		MBeanAttributeInfo[] attributes = null;
		MBeanConstructorInfo[] constructors = null;
		MBeanOperationInfo[] operations = null;
		MBeanNotificationInfo[] notifications = null;
		MBeanInfo beanInfo = new MBeanInfo("mhenche.test.MbeanDemo", "Es un bean de ejemplo", attributes, constructors, operations, notifications);
		return beanInfo;
	}

}
