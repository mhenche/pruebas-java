package com.mhenche.main;

import java.text.DecimalFormat;

public class TestFormatNumber {

	public static void main(String[] args) {
		//Emulación parámetros de entrada:
		final Integer entrada = new Integer(152); //emula la entrada. Sería recuperada de la secuencia.
		final boolean esEmpresa = true;
		
		final String poliza = (esEmpresa)?"SMO19T2002":"SMO19T2006"; //para elegir entre dos, algo como poliza, obviamente serían constantes
		DecimalFormat format = new DecimalFormat("000000"); //el formato que quieras!, lo típico que el patrón esté parametrizado en un properties o YML o una constante java. Yo lo pongo a pelo
		
		//y tenemos la salida
		
		System.out.println(format.format(entrada));
		System.out.println(String.format("Lo que quieras, constante. Ahora un parámetro, %1$s y despues, otro paramáetro, este numérico %2$d",  poliza, entrada));
		//otro ejemplo más parecido al problema. Diría que el método String.format admite también número de ceros para el d, pero lo hago con DecimalFormat y parámetro d
		System.out.println(String.format("%1$s-%2$s",  poliza, format.format(entrada)));

	}

}

