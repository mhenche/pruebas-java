package com.mhenche.main;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class TestJava9 {

	public static void main(String[] args) {
		
		Calendar calendar = GregorianCalendar.getInstance();		
		calendar.set(Calendar.DAY_OF_MONTH, 29);
		Date dia = calendar.getTime();
		

		List<String> formatos = Arrays.asList("yyyy",
				"W", 
				"E",
				"EEEE",
				"u",
				"w",
				"D",
				"F",
				"M",
				"MM",
				"MMMM"
				);
		
		
		formatos.stream()
			.forEach( (a)->{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(a, Locale.ENGLISH);
				System.out.println(a +"  -->  "+simpleDateFormat.format(dia));
				
			});
		
	}

}

