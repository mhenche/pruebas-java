package com.mhenche.main;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import mhenche.test.mdw.DatosCanal;
import mhenche.test.mdw.DatosCliente;
import mhenche.test.mdw.DatosEnvio;
import mhenche.test.mdw.DatosMSISDN;
import mhenche.test.mdw.DatosPersonalizacion;
import mhenche.test.mdw.DatosPersonalizacionEmail;
import mhenche.test.mdw.DatosReenvio;
import mhenche.test.mdw.DatosSAC;
import mhenche.test.mdw.Parametros;
import mhenche.test.mdw.XMLPeticion;

public class TestMDW_Encoding {

	public static void main(String[] args) throws JAXBException, IOException {
		JAXBContext jaxbContext = JAXBContext.newInstance(XMLPeticion.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
//		String encod_out ="ISO-8859-1";
		String encod_out ="ISO-8859-15";
//		String encod_out ="UTF-8";


        marshaller.setProperty(Marshaller.JAXB_ENCODING, encod_out);

//        OutputStreamWriter writer = new OutputStreamWriter(System.out, "ISO-8859-1");
//        marshaller.marshal(address, writer);

        System.out.println("El marshaller está configurado como:"+marshaller.getProperty(Marshaller.JAXB_ENCODING));
		OutputStream outputStream = new FileOutputStream("c:/temp/Pet_77002.xml");
		try {

			DatosCliente datosCliente = new DatosCliente();
			DatosCanal datosCanal = new DatosCanal();
			DatosEnvio datosEnvio = new DatosEnvio();
			DatosMSISDN datosMSISDN = new DatosMSISDN();
			DatosPersonalizacion datosPersonalizacion  = new  DatosPersonalizacion();
			DatosPersonalizacionEmail datosPersonalizacionEmail = new DatosPersonalizacionEmail();
			DatosReenvio datosReenvio = new DatosReenvio();
			DatosSAC datosSAC = new DatosSAC();
			XMLPeticion peticion = new XMLPeticion();
			
			datosEnvio.setIdMensaje("9001507");
			
			datosMSISDN.setMsisdn("646400979");
			datosCanal.setTipoEnvio("SMS");
			datosCanal.setCanal("RYG");
			
			datosPersonalizacion.setNumCampos("1");
			
			List<Parametros> parametros = datosPersonalizacion.getParametros();
//			Parametros parametro = new Parametros();
//			parametro.setNombre("CODIGO DE SOLICITUD");
//			parametro.setValor("1-32787259253");			
//			parametros.add(parametro);
			
			Parametros parametro2 = new Parametros();
			parametro2.setNombre("TEXTO1");
			parametro2.setValor("ñ\u20AC$");
			parametros.add(parametro2);
			
//			datosCliente.setDocumento("51779016J");
//			datosSAC.setIdAgenteSAC("TAPIO270");
			
			peticion.setDatosCanal(datosCanal);
			peticion.setDatosEnvio(datosEnvio);
			peticion.setDatosCliente(datosCliente);
			peticion.setDatosPersonalizacion(datosPersonalizacion);
			peticion.setDatosPersonalizacionEmail(datosPersonalizacionEmail);
			peticion.setDatosReenvio(datosReenvio);
			peticion.setDatosSAC(datosSAC);

			List<DatosMSISDN> lDatosMSISDN = peticion.getDatosMSISDN();
			lDatosMSISDN.add(datosMSISDN);
			
			marshaller.marshal(peticion, System.out);
			marshaller.marshal(peticion, outputStream);
			
		} finally {
			outputStream.close();
		}

		System.out.println("Fin de la prueba");
		System.out.println(Charset.defaultCharset());
		

	}

}


