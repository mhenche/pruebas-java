package com.mhenche.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Formatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class FirmarDocumento {

	private static final String HASH_ALGORITHM = "HmacSHA256";

	public static void main(String[] args) throws SignatureException, FileNotFoundException {
		
		//path to private key file
		String PRIVATE_KEY_FILE_RSA = "C:\\Users\\mhenchea\\Documents\\Orange\\Acceso a máquinas\\id_rsa_mhenchea.pub";
		FileInputStream in = new FileInputStream(PRIVATE_KEY_FILE_RSA);
		// passphrase - the key to decode private key
		String passphrase = "somepass";
		//PKCS8Key pkcs8 = new PKCS8Key(in, passphrase.toCharArray());
//		PKCS8Key pkcs8 = new PKCS8Key(in, passphrase.toCharArray());
//		byte[] decrypted = pkcs8.getDecryptedBytes();
//		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decrypted);
//		RSAPrivateKey privKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(spec);
		
		String prueba = hashMac("Este es el texto que quiero firmar", "1cir=2pirad");
		
		
		System.out.println(prueba);

	}

	/**
	 * Encryption of a given text using the provided secretKey
	 * 
	 * @param text
	 * @param secretKey
	 * @return the encoded string
	 * @throws SignatureException
	 */
	public static String hashMac(String text, String secretKey) throws SignatureException {

		try {
			Key sk = new SecretKeySpec(secretKey.getBytes(), HASH_ALGORITHM);
			Mac mac = Mac.getInstance(sk.getAlgorithm());
			mac.init(sk);
			final byte[] hmac = mac.doFinal(text.getBytes());
			return toHexString(hmac);
		} catch (NoSuchAlgorithmException e1) {
			// throw an exception or pick a different encryption method
			throw new SignatureException("error building signature, no such algorithm in device " + HASH_ALGORITHM);
		} catch (InvalidKeyException e) {
			throw new SignatureException("error building signature, invalid key " + HASH_ALGORITHM);
		}
	}

	public static String toHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);

		Formatter formatter = new Formatter(sb);
		for (byte b : bytes) {
			formatter.format("%02x", b);
		}

		formatter.close();

		return sb.toString();
	}
	
//	public static PrivateKey bigIntegerToPrivateKey(BigInteger e, BigInteger m) {
//	    RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
//	    KeyFactory fact = KeyFactory.getInstance("RSA");
//	    PrivateKey privKey = fact.generatePrivate(keySpec);
//	    return privKey;
//	}

}
