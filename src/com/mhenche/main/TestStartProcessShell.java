package com.mhenche.main;

import java.util.Random;

public class TestStartProcessShell {

	public static void main(String[] args) throws Exception {
		
		System.out.println(
				"Si no has indicado un parámetro numérico, se ejecutará "
				+ "el proceso de forma aleatoria con 3 posibles salida: 0, "
				+ "todo ok, 1, error no controlado o 2 error controlado. \n"
				+ "En el caso de que llames al proceso con un valor numérico, terminará delvoviendo ese valor como código de salida. Util para probar alarmados."
				+ "\n\n\n");
		
		//Esperamos un poco para que no de sesación de que el proceso no arranca si no se ve la consola.
		Thread.sleep(1000);
				
		int queHaPasado = 0;

        if(args.length > 0){//Emulamos un código de salida erroneo
        	queHaPasado = Integer.parseInt(args[0]);        	
        	System.out.println(String.format("caso forzado con valor: %1$s, error controlado. Código de salida: %1$s", queHaPasado));
        }else {
        	TestStartProcessShell processShell = new TestStartProcessShell();
        	queHaPasado = processShell.emulaEjecución();
        }
		
		System.exit(queHaPasado);

	}

	private int emulaEjecución() throws Exception {

		int codSalida = 0;
		Random random = new Random();		
				
		//Emulación de ¿qué ha pasado?. Cogemos un aleatorio.
		double valor = random.nextDouble();		
		if (valor < 0.333d) {//caso de acción no esperada
			System.out.println("caso 1, error NO controlado. Código de salida: 1"); //ojo, aquí no indicamos el código de salida... Sencillamente, terminaos con excepción.
			throw new Exception("Vaya mala suerte, ha salido por debajo de 0.333. Caso no controlado");
		} else if (valor > 0.666d) {//caso de proceso acaba como nos gustaría, haciendo lo que tenía que hacer sin errores.
			System.out.println("caso 0, Sin errores. Código de salida: 0");
			//no damos valor a la salida, es una salida con valor por defecto
		} else {//caso de errores conocidos. Debería haber un tratamiento para cada uno			
			//caso 1, por ejemplo, sin conexión a la BBDD:			
			try {
				System.out.println("caso 2, error controlado. Código de salida: 2");
				throw new Exception("Excepción a controlar.");
			} catch (Exception e) {				
				codSalida = 2;				
			}
		}

		return codSalida;
				

	}

}
