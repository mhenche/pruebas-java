package com.mhenche.main;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestPatterns {

	public static final void main(String[] args) {
		
		String patron ="[0-9]{9}";
		
		 
        Pattern pattern = Pattern.compile(patron); 
  
        // Get the String to be matched 
        String input = "534361805"; 
        String input2 = "53436180q";
        String input3 = "534361803";
  
        // Create a matcher for the input String 
        Matcher matcher  = pattern.matcher(input);
        System.out.println("Cuadra?: " +pattern.matcher(input).matches());
        System.out.println("Cuadra?: " +pattern.matcher(input2).matches());
        System.out.println("Cuadra?: " +pattern.matcher(input3).matches());
        
        // Get the current matcher state 
        MatchResult result  = matcher.toMatchResult(); 
        System.out.println("Current Matcher: "+ result); 
  
		
		
	}
	
}
