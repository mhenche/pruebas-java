/**
 * 
 */
package pdfs;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author mhenche
 *
 */
public class PdfEventos extends PdfPageEventHelper{
	
	
	protected Paragraph cabeceraPagina;
	protected Phrase piePagina;
	
//	static {
//		cabeceraPagina= new Phrase("");
//		piePagina= new Phrase("Este es el pie de la p�gina");
//		Paragraph paragraph = new Paragraph();
//		try {
//			
//			URL url = new URL("file:D:/temp/PI_2.jpg");
//			Image img = Image.getInstance(url);
//			img.setAlignment(Image.LEFT);
//			paragraph.add(img);
//			paragraph.add("Disfrute descargando porno");
//		} catch (BadElementException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		cabeceraPagina.add(paragraph);
////		CompanyLocalServiceUtil.getCompanyById(PortalUtil.getDefaultCompanyId()).getLogoId();
////		PortalUtil.get
//
//		
//	}
	

	/**
	 * 
	 */
	public PdfEventos() {
	
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onChapter(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, float, com.itextpdf.text.Paragraph)
	 */
	@Override
	public void onChapter(PdfWriter writer, Document document,
			float paragraphPosition, Paragraph title) {
		// TODO Auto-generated method stub
		super.onChapter(writer, document, paragraphPosition, title);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onChapterEnd(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, float)
	 */
	@Override
	public void onChapterEnd(PdfWriter writer, Document document, float position) {
		// TODO Auto-generated method stub
		super.onChapterEnd(writer, document, position);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
	 */
	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {
		// TODO Auto-generated method stub
		super.onCloseDocument(writer, document);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
	 */
	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		
		piePagina= new Phrase("Este es el pie de la p�gina");
		
		PdfContentByte  pdfContentByte = writer.getDirectContent();
		ColumnText.showTextAligned(pdfContentByte, Element.ALIGN_CENTER, piePagina,(document.right() - document.left()) / 2
				+ document.leftMargin(),document.bottom()-10,0 );
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onGenericTag(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, com.itextpdf.text.Rectangle, java.lang.String)
	 */
	@Override
	public void onGenericTag(PdfWriter writer, Document document,
			Rectangle rect, String text) {
		// TODO Auto-generated method stub
		super.onGenericTag(writer, document, rect, text);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
	 */
	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		// TODO Auto-generated method stub
		super.onOpenDocument(writer, document);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onParagraph(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, float)
	 */
	@Override
	public void onParagraph(PdfWriter writer, Document document,
			float paragraphPosition) {
		// TODO Auto-generated method stub
		super.onParagraph(writer, document, paragraphPosition);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onParagraphEnd(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, float)
	 */
	@Override
	public void onParagraphEnd(PdfWriter writer, Document document,
			float paragraphPosition) {
		// TODO Auto-generated method stub
		super.onParagraphEnd(writer, document, paragraphPosition);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onSection(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, float, int, com.itextpdf.text.Paragraph)
	 */
	@Override
	public void onSection(PdfWriter writer, Document document,
			float paragraphPosition, int depth, Paragraph title) {
		// TODO Auto-generated method stub
		super.onSection(writer, document, paragraphPosition, depth, title);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onSectionEnd(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document, float)
	 */
	@Override
	public void onSectionEnd(PdfWriter writer, Document document, float position) {
		// TODO Auto-generated method stub
		super.onSectionEnd(writer, document, position);
	}

	/* (non-Javadoc)
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onStartPage(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
	 */
	@Override
	public void onStartPage(PdfWriter writer, Document document) {
		
		cabeceraPagina= new Paragraph();
		Paragraph paragraph = new Paragraph();
		try {
			
			URL url = new URL("file:D:/temp/PI_2.jpg");
			Image img = Image.getInstance(url);
//			img.scaleToFit(500f, 30f);
			//Ancho-alto
			img.scaleAbsolute(30f, 30f);
			//Con esto lo pone en los m�rgenes.
//			img.setAbsolutePosition(document.left(), document.top());
			img.setAlignment(Image.LEFT|Image.TEXTWRAP);
			
//			img.setBorderWidthRight(300f);
//			img.setBorderWidthLeft(300f);
			img.enableBorderSide(Image.RIGHT);
			img.enableBorderSide(Image.LEFT);
//			img.setAlignment(Image.LEFT);
//			paragraph.add(img);
//			paragraph.add("Disfrute descargando porno");
//			cabeceraPagina.add(paragraph);
			document.add(img);
			Phrase ejemplo = new Phrase("Esto es la cabecera");
			document.add(ejemplo);
			
//			cabeceraPagina.add(img);
				
//			cabeceraPagina.add(ejemplo);
//			document.add(cabeceraPagina);
			
//			document.add(ejemplo);
//			document.add(img);
//			document.add(new Phrase("Hola desde el coraz�n"));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	

}
