package pdfs;


import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.Color;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * This example was written by Bruno Lowagie. It is part of the book 'iText in
 * Action' by Manning Publications. 
 * ISBN: 1932394796
 * http://www.1t3xt.com/docs/book.php 
 * http://www.manning.com/lowagie/
 */

public class ColumnWithAddElement {
	public static final Font FONT9 = com.itextpdf.text.FontFactory.getFont(
			com.itextpdf.text.FontFactory.TIMES_ROMAN, 9);

	public static final Font FONT11 = FontFactory.getFont(
			FontFactory.TIMES_ROMAN, 11);

	public static final Font FONT11B = FontFactory.getFont(
			FontFactory.TIMES_ROMAN, 11, Font.BOLD);

	public static final Font FONT14B = FontFactory.getFont(
			FontFactory.TIMES_ROMAN, 14, Font.BOLD);

	public static final Font FONT14BC = FontFactory.getFont(
			FontFactory.TIMES_ROMAN, 14, Font.BOLD);

	public static final Font FONT24B = FontFactory.getFont(FontFactory.TIMES_ROMAN, 24, Font.BOLD);

	/**
	 * Generates a PDF file with columns containing composite content.
	 * 
	 * @param args
	 *            no arguments needed here
	 */
	public static void main(String[] args) {
		System.out.println("Chapter 7: example ColumnWithAddElement");
		System.out.println("-> Creates a PDF file with columns containing composite content.");
		System.out.println("-> jars needed: iText.jar");
		System.out.println("-> extra resource: resources/8001.jpg");
		System.out.println("-> file generated: column_with_add_element.pdf");
		// step 1: creation of a document-object
		com.itextpdf.text.Document document = new Document(PageSize.A4);
		try {
			// step 2:
			// we create a writer
			PdfWriter writer = PdfWriter.getInstance(
			// that listens to the document
					document,
					// and directs a PDF-stream to a file
					new FileOutputStream("d:/temp/prueba.pdf"));
			// step 3: we open the document
			document.open();
			// step 4: we add a table to the document
			float gutter = 20;
			int numColumns = 2;
			float fullWidth = document.right() - document.left();
			float columnWidth = (fullWidth - (numColumns - 1) * gutter)
					/ numColumns;
			float allColumns[] = new float[numColumns]; // left
			for (int k = 0; k < numColumns; ++k) {
				allColumns[k] = document.left() + (columnWidth + gutter) * k;
			}
			PdfContentByte pdfContentByte = writer.getDirectContent();
			
			ColumnText columnText = new ColumnText(pdfContentByte);
			columnText.setLeading(0, 1.5f);
			columnText.setSimpleColumn(document.left(), 0, document.right(), document.top());
			
			Phrase fullTitle = new Phrase("POJOs in Action", FONT24B);
			columnText.addText(fullTitle);
			columnText.go();
//			Phrase subTitle = new Phrase("Developing Enterprise Applications with Lightweight Frameworks",FONT14B);
//			ct.addText(subTitle);
//			ct.go();
			float currentY = columnText.getYLine();
			currentY -= 4;
			pdfContentByte.setLineWidth(1);
			pdfContentByte.moveTo(document.left(), currentY);
			pdfContentByte.lineTo(document.right(), currentY);
			pdfContentByte.stroke();
			columnText.setYLine(currentY);
			columnText.addText(new Chunk("Chris Richardson", FONT14B));
			columnText.go();
			currentY = columnText.getYLine();
			currentY -= 15;
			float topColumn = currentY;
			for (int k = 1; k < numColumns; ++k) {
				float x = allColumns[k] - gutter / 2;
				pdfContentByte.moveTo(x, topColumn);
				pdfContentByte.lineTo(x, document.bottom());
			}
			pdfContentByte.stroke();
			int currentColumn = 0;
			columnText.setSimpleColumn(allColumns[currentColumn], document.bottom(),
					allColumns[currentColumn] + columnWidth, currentY);
			Image img = Image.getInstance("D:/temp/itext/examples/resources/in_action/chapter07/8001.jpg");
			columnText.addElement(img);
			columnText.addElement(newParagraph("Key Data:", FONT14BC, 5));
			PdfPTable ptable = new PdfPTable(2);
			float[] widths = { 1, 2 };
			ptable.setWidths(widths);
			ptable.getDefaultCell().setPaddingLeft(4);
			ptable.getDefaultCell().setPaddingTop(0);
			ptable.getDefaultCell().setPaddingBottom(4);
			ptable.addCell(new Phrase("Publisher:", FONT9));
			ptable.addCell(new Phrase("Manning Publications Co.", FONT9));
			ptable.addCell(new Phrase("ISBN:", FONT9));
			ptable.addCell(new Phrase("1932394583", FONT9));
			ptable.addCell(new Phrase("Price:", FONT9));
			ptable.addCell(new Phrase("$44.95", FONT9));
			ptable.addCell(new Phrase("Page Count:", FONT9));
			ptable.addCell(new Phrase("450", FONT9));
			ptable.addCell(new Phrase("Pub Date:", FONT9));
			ptable.addCell(new Phrase("2005", FONT9));
			ptable.setSpacingBefore(5);
			ptable.setWidthPercentage(100);
			columnText.addElement(ptable);
			columnText.addElement(newParagraph("Description", FONT14BC, 15));
			columnText.addElement(newParagraph(
							"In the past, developers built enterprise Java applications using EJB technologies that are excessively complex and difficult to use. Often EJB introduced more problems than it solved. There is a major trend in the industry towards using simpler and easier technologies such as Hibernate, Spring, JDO, iBATIS and others, all of which allow the developer to work directly with the simpler Plain Old Java Objects or POJOs. Now EJB version 3 solves the problems that gave EJB 2 a black eye--it too works with POJOs.",
							FONT11, 5));
			Paragraph p = new Paragraph();
			p.setSpacingBefore(5);
			p.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk anchor = new Chunk("POJOs in Action", FONT11B);
			anchor.setAnchor("http://www.manning.com/books/crichardson");
			p.add(anchor);
			p.add(new Phrase(
							" describes the new, easier ways to develop enterprise Java applications. It describes how to make key design decisions when developing business logic using POJOs, including how to organize and encapsulate the business logic, access the database, manage transactions, and handle database concurrency. This book is a new-generation Java applications guide: it enables readers to successfully build lightweight applications that are easier to develop, test, and maintain.",
							FONT11));
			columnText.addElement(p);
			columnText.addElement(newParagraph("Inside the Book", FONT14BC, 15));
			List list = new List(List.UNORDERED, 15);
			ListItem li;
			li = new ListItem("How to develop apps in the post EJB 2 world",
					FONT11);
			list.add(li);
			li = new ListItem(
					"How to leverage the strengths and work around the weaknesses of: JDO, Hibernate, and EJB 3",
					FONT11);
			list.add(li);
			li = new ListItem("How to benefit by using aspects", FONT11);
			list.add(li);
			li = new ListItem(
					"How to do test-driven development with lightweight frameworks",
					FONT11);
			list.add(li);
			li = new ListItem("How to accelerate the edit-compile-debug cycle",
					FONT11);
			list.add(li);
			columnText.addElement(list);
			columnText.addElement(newParagraph("About the Author...", FONT14BC, 15));
			columnText.addElement(newParagraph(
							"Chris Richardson is a developer, architect and mentor with over 20 years of experience. He runs a consulting company that jumpstarts new development projects and helps teams that are frustrated with enterprise Java become more productive and successful. Chris has been a technical leader at a variety of companies including Insignia Solutions and BEA Systems. Chris holds a MA & BA in Computer Science from the University of Cambridge in England. He lives in Oakland, CA.",
							FONT11, 15));
			while (true) {
				int status = columnText.go();
				if (!ColumnText.hasMoreText(status))
					break;
				// we run out of column. Let's go to another one
				++currentColumn;
				if (currentColumn >= allColumns.length)
					break;
				columnText.setSimpleColumn(allColumns[currentColumn],
						document.bottom(), allColumns[currentColumn]
								+ columnWidth, topColumn);
			}
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		// step 5: we close the document
		document.close();
	}

	private static Paragraph newParagraph(String s, Font f, float spacingBefore) {
		Paragraph p = new Paragraph(s, f);
		p.setAlignment(Element.ALIGN_JUSTIFIED);
		p.setSpacingBefore(spacingBefore);
		return p;
	}
}