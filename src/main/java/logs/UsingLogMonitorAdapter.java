package logs;

/*
 * Copyright 1999-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.lf5.LogLevel;
import org.apache.log4j.lf5.util.AdapterLogRecord;
import org.apache.log4j.lf5.util.LogMonitorAdapter;

/**
 * This class is a simple example of how use the LogMonitorAdapter to bypass the
 * Log4JAppender and post LogRecords directly to the LogMonitor
 * 
 * To make this example work, ensure that the lf5.jar and lf5-license.jar files
 * are in your classpath, and then run the example at the command line.
 * 
 * @author Richard Hurst
 */

// Contributed by ThoughtWorks Inc.

public class UsingLogMonitorAdapter {
	// --------------------------------------------------------------------------
	// Constants:
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Protected Variables:
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Private Variables:
	// --------------------------------------------------------------------------
	private static Logger _adapter;

	private static Logger logger1 = Logger
			.getLogger(UsingLogMonitorAdapter.class);
	private static Logger logger2 = Logger.getLogger("TestClass.Subclass");
	private static Logger logger3 = Logger
			.getLogger("TestClass.Subclass.Subclass");

	static {
		_adapter = Logger.getLogger(UsingLogMonitorAdapter.class);
		// LogMonitorAdapter.newInstance(LogMonitorAdapter.LOG4J_LOG_LEVELS);
	}

	// --------------------------------------------------------------------------
	// Constructors:
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Public Methods:
	// --------------------------------------------------------------------------

	public static void main(String[] args) {
		UsingLogMonitorAdapter test = new UsingLogMonitorAdapter();
		test.doMyBidding();
	}

	public void doMyBidding() {

		// String resource =
		// "/examples/lf5/InitUsingMultipleAppenders/example.properties";
		// URL configFileResource =
		// UsingLogMonitorAdapter.class.getResource(resource);
		// PropertyConfigurator.configure(configFileResource);

		String resource = "file:D:/workSpaceLifeRay/ext/ext-impl/src/log4j.properties";
		URL configFileResource = UsingLogMonitorAdapter.class
				.getResource(resource);
		PropertyConfigurator.configure(configFileResource);

		// Add a bunch of logging statements ...
		logger1.debug("Hello, my name is Homer Simpson.");
		logger1.debug("Hello, my name is Lisa Simpson.");
		logger2.debug("Hello, my name is Marge Simpson.");
		logger2.debug("Hello, my name is Bart Simpson.");
		logger3.debug("Hello, my name is Maggie Simpson.");

		logger2.info("We are the Simpsons!");
		logger2.info("Mmmmmm .... Chocolate.");
		logger3.info("Homer likes chocolate");
		logger3.info("Doh!");
		logger3.info("We are the Simpsons!");

		logger1.warn("Bart: I am through with working! Working is for chumps!"
				+ "Homer: Son, I'm proud of you. I was twice your age before "
				+ "I figured that out.");
		logger1.warn("Mmm...forbidden donut.");
		logger1.warn("D'oh! A deer! A female deer!");
		logger1.warn("Truly, yours is a butt that won't quit."
				+ "- Bart, writing as Woodrow to Ms. Krabappel.");

		logger2.error("Dear Baby, Welcome to Dumpsville. Population: you.");
		logger2.error("Dear Baby, Welcome to Dumpsville. Population: you.",
				new IOException("Dumpsville, USA"));
		logger3.error("Mr. Hutz, are you aware you're not wearing pants?");
		logger3.error("Mr. Hutz, are you aware you're not wearing pants?",
				new IllegalStateException("Error !!"));

		logger3.fatal("Eep.");

		logger3.fatal("Mmm...forbidden donut.", new SecurityException(
				"Fatal Exception ... "));

		logger3.fatal("D'oh! A deer! A female deer!");
		logger2.fatal("Mmmmmm .... Chocolate.", new SecurityException(
				"Fatal Exception"));

		// Put the main thread is put to sleep for 5 seconds to allow the
		// SocketServer to process all incoming messages before the Socket is
		// closed. This is done to overcome some basic limitations with the
		// way the SocketServer and SocketAppender classes manage sockets.
		try {
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException ie) {
		}

	}
	// --------------------------------------------------------------------------
	// Protected Methods:
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Private Methods:
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Nested Top-Level Classes or Interfaces:
	// --------------------------------------------------------------------------
}
