package prueba;

public class Pojo
{

	private String valor1;
	private Integer valor2;
	/**
	 * @return the valor1
	 */
	public String getValor1()
	{
		return valor1;
	}
	/**
	 * @param valor1 the valor1 to set
	 */
	public void setValor1(String valor1)
	{
		this.valor1 = valor1;
	}
	/**
	 * @return the valor2
	 */
	public Integer getValor2()
	{
		return valor2;
	}
	/**
	 * @param valor2 the valor2 to set
	 */
	public void setValor2(Integer valor2)
	{
		this.valor2 = valor2;
	}
	
}
