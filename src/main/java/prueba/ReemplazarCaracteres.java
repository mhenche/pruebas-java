/**
 * 
 */
package prueba;

import java.util.regex.Pattern;

/**
 * @author mhenche
 *
 */
public class ReemplazarCaracteres {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		String entrada2 = "<scrIpt>alert('a')</ScriPt>";
		
		String entrada2 = "1235aa555";

		
		//Pattern patron = Pattern.compile("<script>|</script>",Pattern.CASE_INSENSITIVE);
		Pattern patron = Pattern.compile("\\{d}",Pattern.CASE_INSENSITIVE);
		
		
		//System.out.println(patron.matcher(entrada2).replaceAll(""));
		
		System.out.println("1".matches(patron.pattern()));
		System.out.println("1654968721".matches(patron.pattern()));
		System.out.println("asdd".matches(patron.pattern()));
		System.out.println("asas122".matches(patron.pattern()));
		System.out.println("1212aaa".matches(patron.pattern()));
		System.out.println("$5".matches(patron.pattern()));
		
		System.exit(0);

	}

}
