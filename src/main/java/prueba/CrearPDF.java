package prueba;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.xml.sax.Attributes;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.HtmlTags;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;

import pdfs.PdfEventos;


public class CrearPDF {
	static Document document = new Document(PageSize.A4);

	/**
	 * @param args
	 */
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {

		/* chapter03/HelloWorldMaximum.java */

//		Document document = new Document(PageSize.A4);
		PdfWriter writer;
		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(
					"D:/temp/prueba.pdf"));
			
//			Properties systemProperties = System.getProperties();
//			systemProperties.setProperty("http.proxyHost","proxy.indra.es");
//			systemProperties.setProperty("http.proxyPort","8080");
//			Authenticator.setDefault(new Authenticator() {
//				protected PasswordAuthentication getPasswordAuthentication() {
//			        return new
//			           PasswordAuthentication("mhenche","matematico105$".toCharArray());
//			    }
//
//			});
//			systemProperties.setProperty("http.password","matematico105$".toCharArray());
//			systemProperties.setProperty("http.username","mhenche");
			

			
			writer.setPdfVersion(PdfWriter.VERSION_1_6);
			writer.setViewerPreferences(PdfWriter.PageLayoutOneColumn);
			
			//Esto se puede utilizar para dar formato.
			writer.setPageEvent(new PdfEventos());
			
			
			document.open();
			document.newPage();
			document.add(new Paragraph("A4 page Size--->>roseinia.net"));
			
			
			
			try {
				URL url = new URL("file:D:/temp/plantilla.html");
				
				
				
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				List<Element> elementlist = HTMLWorker.parseToList(in, null);
				for (Element element :elementlist) {
					document.newPage();
				    document.add(element);
				}
//				Image img = Image.getInstance(new URL("http://localhost:8080/image/image_gallery?img_id=522724"));
//				System.out.println(img);
//				document.add(img);
				
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			document.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}