package prueba;





public class EventoMapa {
	private String detalle;
	private String titulo;
	
	
	public class Lugar  {
		String nom;
		float lat;
		float lon;		
		Evento []lugar;
		public class Evento{
			private String formato = "'Del ' yyyyDD";
			long id;
			String titulo;
			String fechas;
			/**
			 * @return the formato
			 */
			public final String getFormato() {
				return formato;
			}
			/**
			 * @param formato the formato to set
			 */
			public final void setFormato(String formato) {
				this.formato = formato;
			}
			/**
			 * @return the id
			 */
			public final long getId() {
				return id;
			}
			/**
			 * @param id the id to set
			 */
			public final void setId(long id) {
				this.id = id;
			}
			/**
			 * @return the titulo
			 */
			public final String getTitulo() {
				return titulo;
			}
			/**
			 * @param titulo the titulo to set
			 */
			public final void setTitulo(String titulo) {
				this.titulo = titulo;
			}
			/**
			 * @return the fechas
			 */
			public final String getFechas() {
				return fechas;
			}
			/**
			 * @param fechas the fechas to set
			 */
			public final void setFechas(String fechas) {
				this.fechas = fechas;
			}
			
		}
		/**
		 * @return the nom
		 */
		public final String getNom() {
			return nom;
		}
		/**
		 * @param nom the nom to set
		 */
		public final void setNom(String nom) {
			this.nom = nom;
		}
		/**
		 * @return the lat
		 */
		public final float getLat() {
			return lat;
		}
		/**
		 * @param lat the lat to set
		 */
		public final void setLat(float lat) {
			this.lat = lat;
		}
		/**
		 * @return the lon
		 */
		public final float getLon() {
			return lon;
		}
		/**
		 * @param lon the lon to set
		 */
		public final void setLon(float lon) {
			this.lon = lon;
		}
		/**
		 * @return the lugar
		 */
		public final Evento[] getLugar() {
			return lugar;
		}
		/**
		 * @param lugar the lugar to set
		 */
		public final void setLugar(Evento[] lugar) {
			this.lugar = lugar;
		}
	}
	/**
	 * @return the detalle
	 */
	public final String getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public final void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	/**
	 * @return the titulo
	 */
	public final String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public final void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
