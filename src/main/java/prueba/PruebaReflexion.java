package prueba;

import java.lang.reflect.Method;

public class PruebaReflexion
{

	public static void main(String [] args) {
		
		Pojo pojo = new Pojo();
		Class clase = pojo.getClass();		
		for(Method method : clase.getMethods() ) {
			System.out.println(method.getName());
		}
		
	}
}
