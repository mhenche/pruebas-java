package com.mhenche.pruebas.java8.math;

import java.util.Objects;

public class Calculator {
	
	@FunctionalInterface
	interface IntegerMath {
        int operation(int a, int b);   
        static int operation2(int a, int b){
        	return a+b;
        }
        //Compila por que es un método de la clase Object.
        String toString();
        
        
//      Esto no compilaría por que no puede haber más de un método abstracto en un FunctionalInterface
//        String toString(String a);
        
    }
	
	public int operateBinary(int a, int b, IntegerMath op) {
        return op.operation(a, b);
    }
	
	

	public static void main(String[] args) {
		
		Calculator myApp = new Calculator();
		
		//creamos  instancias clase que no existen, sólo con la definición del interface
        IntegerMath addition = (a, b) -> a + b;        
        IntegerMath subtraction = (a, b) -> a - b;
        
        System.out.println("40 + 2 = " + myApp.operateBinary(40, 2, addition));
        System.out.println("20 - 10 = " +  myApp.operateBinary(20, 10, subtraction));
        
        

	}

}
