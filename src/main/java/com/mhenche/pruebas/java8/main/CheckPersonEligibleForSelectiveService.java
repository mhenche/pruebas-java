package com.mhenche.pruebas.java8.main;

import java.util.function.Predicate;

public class CheckPersonEligibleForSelectiveService implements Predicate<Person> {
    public boolean test(Person p) {
        return p.gender == Person.Sex.MALE &&
            p.getAge() >= 10 &&
            p.getAge() <= 50;
    }
}