package com.mhenche.pruebas.java8.main;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.mhenche.pruebas.java8.main.Person.Sex;

public class Prueba {

	public static void main(String[] args) {

		// Generamos una lista para pruebas.
		List<Person> lPersonas = new ArrayList<>();
		for (int i = 1; i < 12; i++) {
			LocalDate nacimiento = LocalDate.of((int) Math.round(1900 + 116 * Math.random()), i, i);
			
			Sex genero = Math.random() > 0.5 ? Sex.FEMALE : Sex.MALE;
			String email = "mhenche" + i + "@indra.es";
			Person e = new Person("Nombre" + i, nacimiento, genero, email);
			lPersonas.add(e);
		}

		// Hacemos las pruebas

		// printPersonsWithinAgeRange(roster, 0,100);
		// printPersonsOlderThan(roster, 50);

		//Podemos crear una clase que dice si sí o si no
//		CheckPerson tester = new CheckPersonEligibleForSelectiveService();
//		printPersons(roster, tester);

		//Aquí lo que hacemos es crear una clase anónima, es decir, la creamos en el momento de usarla.
//		printPersons(roster, new CheckPerson() {
//			public boolean test(Person p) {
//				return p.getGender() == Person.Sex.MALE && p.getAge() >= 18 && p.getAge() <= 25;
//			}
//		});
		
		//Con lamba expresion
		printPersons(lPersonas, (Person p) -> p.getGender() == Person.Sex.MALE);
		
		
		//con lamdba expresión genérica.
//		printPersonsGeneric(roster, pred);
		

	}

	public static void printPersons(List<Person> roster, CheckPerson tester) {
		for (Person p : roster) {
			if (tester.test(p)) {
				p.printPerson();
			}
		}
	}
	
	public static void printPersonsGeneric(List<Person> roster, Predicate<Person> pred ) {
		for (Person p : roster) {
			if (pred.test(p)) {
				p.printPerson();
			}
		}
	}

	public static void printPersonsWithinAgeRange(List<Person> roster, int low, int high) {
		for (Person p : roster) {
			if (low <= p.getAge() && p.getAge() < high) {
				p.printPerson();
			}
		}
	}

	public static void printPersonsOlderThan(List<Person> roster, int age) {
		for (Person p : roster) {
			if (p.getAge() >= age) {
				p.printPerson();
			}
		}
	}
}
