package com.mhenche.pruebas.java8;

import java.util.Collections;
import java.util.Comparator;

@FunctionalInterface
public interface IpruebaFunctionalInterface<T> {


	
	abstract String unicAbstractMethod();
	
	static String otroMetodo() {
		
		return "";
	}
	
	static String otroMetodo2() {
		return "";
	}
	

	// Puedo tener dos métodos, por que el toString ya está en el objeto Object
	String toString();

	//Este no afecta por que no es abstracto.
	public static <T extends Comparable<? super T>> Comparator<T> reverseOrder() {
		return Collections.reverseOrder();
	}

}
