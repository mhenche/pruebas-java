package com.mhenche.pruebas.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * Ejemplo de envio de correo simple con JavaMail
 *
 * @author Chuidiang
 *
  */
public class EnviarMail
{
    /**
     * main de prueba
     * @param args Se ignoran.
     */
    public static void main(String[] args)
    {
        try
        {
        	
        	String destinatario = "miguelangel.henche@orange.com";
//        	
        	
        	
        	
        	

      	  
//      			  
            // Propiedades de la conexión para gmail
        	String pass = "matematico101$";
        	String from = "miguel.henche@gmail.com";
        	String host = "smtp.gmail.com";

            Properties props = new Properties();
            props.setProperty("mail.smtp.host", host);
            //props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", from);
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.debug", "true");
            props.setProperty("mail.smtp.password", pass);
            
            
//            Propiedades de la conexión para mi empresa
//        	String pass = "Ylkm..,..!1";
//        	String from = "miguelangel.henche@orange.com";
//        	String host = "smtp.orange.com";

//        	Properties props = new Properties();
//            props.setProperty("mail.smtp.host", host);
//            props.setProperty("mail.smtp.port", "25");
//            props.setProperty("mail.smtp.user", from);
//            props.setProperty("mail.smtp.auth", "true");
//            props.setProperty("mail.debug", "true");
//            props.setProperty("mail.smtp.password", pass);

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);
            session.setDebug(true);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            
			message.addRecipient(
                Message.RecipientType.TO,
                new InternetAddress(destinatario));
            message.setSubject("Hola");
            message.setText("Mensajito con Java Mail" + "de los buenos." + "poque si");

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect(host, from, pass);
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}