package mhenche.pruebasMatematicas;

import java.math.BigDecimal;

import org.apache.commons.math3.util.Precision;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		
		
		
		double e = 102d;

		for (int i = 1; i < 10; i++) {
			double aux = e + 0.1 * i;

			// System.out.printf("El Número %1f redondeado es: €2f\n", aux,
			// Math.round(aux));
			System.out.print(aux + "   ");
			System.out.print(Math.round(aux)+"   ");
			System.out.print(Precision.round(aux, 0, BigDecimal.ROUND_HALF_EVEN)+ "   ");

			System.out.println();
			
			
			
			

		}

	}
}
