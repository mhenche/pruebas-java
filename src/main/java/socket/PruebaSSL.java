package socket;

import java.io.PrintStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class PruebaSSL {

	/**
	 * @param args
	 * -Djavax.net.ssl.trustStore=C:/java/jdk1.6.0_04/jre/lib/security/cacerts
		-Djavax.net.ssl.trustStorePassword=popelle
	-Djavax.net.debug=all
	 */

	public static void main(String args[]) throws Exception {
		
//		Properties properties= System.getProperties();
//		for(Iterator<Object> it =properties.keySet().iterator();it.hasNext();) {
//			Object clave = it.next();
//			System.out.println(clave+":"+properties.get(clave));
//			
//		}
	    System.setProperty("javax.net.ssl.trustStore", "C:/java/jdk1.6.0_04/jre/lib/security/cacerts");
	    System.setProperty("javax.net.ssl.trustStorePassword", "popelle");
	    System.setProperty("javax.net.debug", "all");
	    System.setProperty("javax.net.ssl.keyStore", "C:/java/jdk1.6.0_04/jre/lib/security/cacerts");
	    System.setProperty("javax.net.ssl.keyStorePassword", "popelle");
	    
	    
	    SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
	    ServerSocket ss = ssf.createServerSocket(25);
	    while (true) {
	      Socket s = ss.accept();
	      SSLSession session = ((SSLSocket) s).getSession();
	      Certificate[] cchain2 = session.getLocalCertificates();
	      for (int i = 0; i < cchain2.length; i++) {
	        System.out.println(((X509Certificate) cchain2[i]).getSubjectDN());
	      }
	      System.out.println("Peer host is " + session.getPeerHost());
	      System.out.println("Cipher is " + session.getCipherSuite());
	      System.out.println("Protocol is " + session.getProtocol());
	      System.out.println("ID is " + new BigInteger(session.getId()));
	      System.out.println("Session created in " + session.getCreationTime());
	      System.out.println("Session accessed in " + session.getLastAccessedTime());

	      PrintStream out = new PrintStream(s.getOutputStream());
	      out.println("Hi");
	      out.close();
	      s.close();
	    }

	  }
}